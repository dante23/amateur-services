SHELL=cmd.exe

# start all containers in the background without forcing build
up:
	@echo Starting docker images...
	docker-compose up -d
	@echo Docker images started!

# stop services then build containers then start the services of amateur services
up_amateur_service: get_amateur_repo
	@echo stopping docker images ( if running... )
	docker-compose down
	@echo building (when required) and starting docker images ...
	docker-compose up --build -d
	@echo docker images build and started. ( amateur services started! )

# migration-amateur.git database building amateur repo services
# fe-amateur-template.git template of fe amateur repo services
# fe-amateur.git fe amateur repo services
get_amateur_repo:
	@echo getting amateur repo
	@echo getting database building amateur repo services
	chdir ..\ && git clone https://gitlab.com/dante23/migration-amateur.git
	@echo getting template of fe amateur repo services
	chdir ..\ && git clone https://gitlab.com/dante23/fe-amateur-template.git
	@echo getting fe amateur repo services
	chdir ..\ && git clone https://gitlab.com/dante23/fe-amateur.git
	@echo done! repo amateur services collected
